import { defineConfig } from 'vite'
const path = require('path')
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [vue()],
    build: {
        lib: {
            entry: path.resolve(__dirname, 'src/index.ts'),
            name: 'TreeDir',
            fileName: (format) => `treedir.${format}.js`,
            formats: ["es", "umd", "cjs"]
            
        },
        rollupOptions: {
            external: ['vue'],
            output: {
                // Provide global variables to use in the UMD build
                // for externalized deps
                inlineDynamicImports: true,
                globals: {
                    vue: 'Vue'
                }
            }
        }
    }
})
