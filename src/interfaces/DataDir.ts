import {NodeTree} from './NodeTree';

export interface DataDir {
	directory: string;
	url?: string;
	base_node: string;
	extensions?: string;
	list_extensions?: Array<string>;
	refresh?: number;
	tree: NodeTree;
}
