export type NodeType  = "file" | "directory"

export interface Options {
	url: string,
	size: string,
	created: Date,
	slug: string
}

export interface NodeTree {
	idx: string,
	name: string,
	type: NodeType,
	options?: Options,
	children: Array<NodeTree>
}
